import React, { useState, useEffect } from "react";
import "./App.css";
import logo from "./moviesAndLogo.png"
import SearchIcon from "./search.svg"
import MovieCard from "./MovieCard";

const API_URL = `http://www.omdbapi.com/?i=tt3896198&apikey=${process.env.REACT_APP_API_KEY}`;

// const movie1 = {
//   "Title": "Hocus Pocus",
//   "Year": "1993",
//   "imdbID": "tt0107120",
//   "Type": "movie",
//   "Poster": "N/A"
// }

const App = () => {
  const [movies, setMovies] = useState([]);              
  const [searchTerm, setSearchTerm] = useState("");              
  
  // call function to fetch movies by title
  const searchMovies = async (title) => {
    // call api_url
    const response = await fetch(`${API_URL}&s=${title}`);
    const data = await response.json();

    setMovies(data.Search);
  }

  useEffect(() => {
    // searchMovies('The Matrix');
  }, []);

  return (
    <div className="app">
      <img
        src={logo}
        alt="Movies & logo"
        height="245" 
      />

      <div className="search">
        <input
          placeholder="Search for a title"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <img
          src={SearchIcon}
          alt="search"
          onClick={() => searchMovies(searchTerm)}
        />
      </div>

      {movies?.length > 0 ? (
        <div className="container">
          {movies.map((movie) => (
            <MovieCard movie={movie} />
          ))}
        </div> 
      ) : (
        <div className="empty">
          <h2>Movie(s) Not Found</h2>
        </div>
      )
      }

    </div>
  );
}

export default App;